package main

import (
	tb "gopkg.in/tucnak/telebot.v2"
	"log"
	"os"
)

func main() {
	botToken := os.Getenv("TELEGRAM_BOT_TOKEN")
	if botToken == "" {
		log.Fatal("missing TELEGRAM_BOT_TOKEN")
	}
	secureRandomPath := os.Getenv("SECURE_RANDOM_PATH")
	if secureRandomPath == "" {
		log.Fatal("missing SECURE_RANDOM_PATH")
	}
	publicURL := os.Getenv("PUBLIC_URL")
	if publicURL == "" {
		log.Fatal("missing the public URL in the format 'https://tgbot.frustrat.es:8443/'")
	}
	listenPort := os.Getenv("LISTEN_PORT")
	if listenPort == "" {
		log.Fatal("missing LISTEN_PORT")
	}
	tlsEnable := os.Getenv("TLS_ENABLE")

	var tlsCertPath, tlsKeyPath string
	var webhook tb.Webhook
	if tlsEnable == "" {
		log.Printf("no tls enabled")
		webhook = tb.Webhook{
			Listen: "0.0.0.0:" + listenPort,
			Endpoint: &tb.WebhookEndpoint{
				PublicURL: publicURL + secureRandomPath,
			},
		}
	} else {
		log.Printf("tls enabled")
		tlsCertPath = os.Getenv("TLS_CERT_PATH")
		if tlsCertPath == "" {
			tlsCertPath = "./tls/cert.pem"
			log.Printf("no tls cert path defined - using " + tlsCertPath)
		}
		tlsKeyPath = os.Getenv("TLS_KEY_PATH")
		if tlsKeyPath == "" {
			tlsKeyPath = "./tls/key.pem"
			log.Printf("no tls key path defined - using " + tlsKeyPath)
		}
		webhook = tb.Webhook{
			Listen: "0.0.0.0:" + listenPort,
			TLS: &tb.WebhookTLS{
				Key:  tlsKeyPath,
				Cert: tlsCertPath,
			},
			Endpoint: &tb.WebhookEndpoint{
				PublicURL: publicURL + secureRandomPath,
				Cert:      tlsCertPath,
			},
		}
	}

	bot, err := tb.NewBot(tb.Settings{
		Token: botToken,
		// Verbose: true,
		Poller: &webhook,
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	//x, e := bot.GetWebhook()
	//log.Printf("%v,%v", x, e)
	//bot.Handle(tb.OnText, func(msg *tb.Message) {
	//	log.Printf("%d", time.Now().Unix())
	//	log.Printf("%d", msg.Unixtime)
	//})

	bot.Handle(tb.OnUserJoined, func(msg *tb.Message) {
		if msg.UserJoined.IsBot {
			member, err := bot.ChatMemberOf(msg.Chat, msg.UserJoined)
			if err != nil {
				log.Printf("could not get ChatMember %d: %v", msg.Sender.ID, err)
			}
			bot.Ban(msg.Chat, member)
		}
		bot.Delete(msg)
	})
	bot.Handle(tb.OnUserLeft, func(msg *tb.Message) {
		bot.Delete(msg)
	})
	bot.Start()
}
