# telegram bouncer

This small tool deletes 'user joined' and 'user left' messages within groups. It also bans bots from joining the group. 

For using the tool create a new bot using the [botFather](https://t.me/BotFather ). Following env vars are available within the docker container:

- `TELEGRAM_BOT_TOKEN` (necessary) - insert the token from the botFather
- `SECURE_RANDOM_PATH` (necessary) - random string for securing the application
- `PUBLIC_URL` (necessary) - provide the public url in this format https://bouncer.gaengeviertel.tk:8443/
- `LISTEN_PORT` (necessary) - used port by the backend
- `TLS_ENABLE` - enable tls for the backend
- `TLS_CERT_PATH` - path to the cert.pem (default `./tls/cert.pem` )
- `TLS_KEY_PATH` - path to the key.pem (default `./tls/key.pem`)

The bot needs to be able to delete messages within the group.